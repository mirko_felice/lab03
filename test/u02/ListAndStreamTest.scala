package u02

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u02.Modules.Person
import u02.Modules.Person.{Student, Teacher}
import u03.Lists.List.Cons
import u03.Lists.List.Nil
import u03.Lists.List
import u03.Streams.Stream
import u03.MoreOnLists.{drop, filter, flatMap, foldLeft, foldRight, map, max, teacherCourses}
import u02.Optionals.Option._

class ListAndStreamTest {

  private val list = Cons(10, Cons(20, Cons(30, Nil())))

  @Test def testDrop() {
    assertEquals(Cons(20, Cons(30, Nil())), drop(list, 1))
    assertEquals(Cons(30, Nil()), drop(list, 2))
    assertEquals(Nil(), drop(list, 5))
  }

  @Test def testFlatMap() {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(list)(v => Cons(v+1, Nil())))
    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))), flatMap(list)(v => Cons(v+1, Cons(v+2, Nil()))))
  }

  @Test def testMap() {
    assertEquals(Cons("10test", Cons("20test", Cons("30test", Nil()))), map(list)(_+"test"))
    assertEquals(Cons(20, Cons(40, Cons(60, Nil()))), map(list)(_*2))
  }

  @Test def testFilter() {
    assertEquals(Cons(30, Nil()), filter(list)(_>25))
    assertEquals(Cons(10, Cons(30, Nil())), filter(list)(_!=20))
  }

  @Test def testMax() {
    assertEquals(Some(25), max(Cons(25, Cons(20, Nil()))))
    assertEquals(None(), max(Nil()))
    assertEquals(Some(50), max(Cons(-5, Cons(50, Nil()))))
  }

  @Test def testTeacherCourses() {
    val student1 = Student("mirko", 1998)
    val teacher1 = Teacher("mario", "LCMC")
    val people1: List[Person] = Cons(student1, Cons(teacher1, Nil()))
    assertEquals(Cons("LCMC", Nil()), teacherCourses(people1))
    val student2 = Student("chiara", 1999)
    val teacher2 = Teacher("mirko", "PPS")
    val people2: List[Person] = Cons(student1, Cons(student2, Nil()))
    assertEquals(Nil(), teacherCourses(people2))
    val people3: List[Person] = Cons(student1, Cons(teacher2, Cons(teacher1, Cons(student2, Nil()))))
    assertEquals(Cons("PPS", Cons("LCMC", Nil())), teacherCourses(people3))
  }

  @Test def testFoldRight() {
    val list = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
    assertEquals(-8, foldRight(list)(0)(_-_))
  }

  @Test def testFoldLeft() {
    val list = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
    assertEquals(-16, foldLeft(list)(0)(_-_))
  }

  @Test def testStreamDrop() {
    val s = Stream.take(Stream.iterate(0)(_+1))(10)
    assertEquals(Cons(6, Cons(7, Cons(8, Cons(9, Nil())))), Stream.toList(Stream.drop(s)(6)))
  }

  @Test def testConstant() {
    assertEquals(Cons("x", Cons("x", Cons("x", Cons("x", Cons("x", Nil()))))), Stream.toList(Stream.take(Stream.constant("x"))(5)))
  }

  @Test def testFibonacci() {
    val fibs: Stream[Int] = Stream.fibonacci()
    assertEquals(Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Nil())))))))), Stream.toList(Stream.take(fibs)(8)))
  }
}
