package u03

import u02.Modules.Person
import u02.Modules.Person.Teacher
import u02.Optionals.Option
import u02.Optionals.Option._
import u03.Lists.List
import u03.Lists.List._

import scala.annotation.tailrec

object MoreOnLists {

  @tailrec
  def drop[A](l: List[A], n: Int): List[A] = l match {
    case Cons(_, t) if n > 0 => drop(t, n-1)
    case _ => l
  }

  def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
    case Cons(h, t) => append(f(h), flatMap(t)(f))
    case Nil() => Nil()
  }

  def map[A,B](l: List[A])(f: A => B): List[B] = flatMap(l)(v => Cons(f(v), Nil()))

  def filter[A](l: List[A])(pred: A => Boolean): List[A] = flatMap(l)(v => {
    if (pred(v)) Cons(v, Nil())
    else Nil()
  })

  def max(l: List[Int]): Option[Int] = l match {
    case Cons(h, t) if h > getOrElse(max(t), 0) => Some(h)
    case Cons(_, t) => max(t)
    case Nil() => None()
  }

  def teacherCourses(people: List[Person]): List[String] = flatMap(people) {
    case Teacher(_, course) => Cons(course, Nil())
    case _ => Nil()
  }

  def foldRight[A,B](l: List[A])(base: => B)(operation: (A,B) => B): B = l match {
    case Cons(h, t) => operation(h, foldRight(t)(base)(operation))
    case Nil() => base
  }

  def foldLeft[A,B](l: List[A])(base: => B)(operation: (B,A) => B): B = l match {
    case Cons(h, t) => foldLeft(t)(operation(base, h))(operation)
    case Nil() => base
  }

}
